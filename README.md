# private-cloud-config

Config management for my private cloud

## Setup
Clone this repo, install Ansible, and execute the playbook file like this:
`ansible-playbook playbook.yml`

## Backup & Restore
Backups are done using restic. Cron jobs trigger the creation of backups and backup rotation.
The backup script expects a file at /root/restic-secrets. Check the script to see which environment variables should be set in it.
Please note that a restic repository needs to be initialized by hand before restic can successfully create backups.

### Restore Nextcloud database
To restore the Nextcloud database use this command:
`cat /root/nextcloud_db_dump.sql | docker exec -i postgres psql -U nextcloud -d nextcloud`
