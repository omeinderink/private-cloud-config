#!/bin/bash

set -eo pipefail

# create a file named restic_secrets in /root 
# and export values for the following environment variables in it:
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_DEFAULT_REGION
# RESTIC_REPOSITORY
# RESTIC_PASSWORD

source /root/restic_secrets

# it's necessary to specify $RESTIC_REPOSITORY with the --repo flag only when initializing
# this can be done manually as it only needs to be done once
# restic init --repo $RESTIC_REPOSITORY

# actually take the backup
restic backup \
  $(docker volume inspect nextcloud | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect snappymail | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect certs | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect letsencrypt | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect mail | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect mail_users | jq .[].Mountpoint | sed 's/"//g') \
  $(docker volume inspect opendkim_keys | jq .[].Mountpoint | sed 's/"//g') \
  /var/mail \
  /var/www \
  /root/nextcloud_db_dump.sql
