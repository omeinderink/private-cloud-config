#!/bin/bash

set -e

# create a file named restic_secrets in /root 
# and export values for the following environment variables in it:
# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_DEFAULT_REGION
# RESTIC_REPOSITORY
# RESTIC_PASSWORD

source /root/restic_secrets

# Rotate backups
restic forget -d 5 -w 2
